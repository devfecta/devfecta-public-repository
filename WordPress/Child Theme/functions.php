<?php
/* Basic custom functions for the Child Theme */

// Sets a Default Profile Photo
add_filter( 'avatar_defaults', 'default_gravatar' );
function default_gravatar ($avatar_defaults) {
	$myavatar = get_site_url().'/wp-content/uploads/favicon.png';
	$avatar_defaults[$myavatar] = "<Client Name>";
	return $avatar_defaults;
}
// Sets Custom Footer on Admin Side
add_filter('admin_footer_text', 'change_admin_footer');
function change_admin_footer() {
	echo '<span id="footer-note">Copyright ' . date('Y') . ' <a href="'. home_url() .'" target="_blank"> <Client Name> </a>.</span>';
}
// Loads Child Stylesheet
add_action( 'wp_enqueue_scripts', 'load_stylesheets');
function load_stylesheets() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( '<Parent Theme Name>-style', get_stylesheet_directory_uri() . '/style.css', array( 'parent-style' ) );
}
// WordPress Login, Registration, and Forgot Password Screen Style Sheet START
add_filter( 'login_headerurl', 'my_login_logo_url' );
function my_login_logo_url() {
    return home_url();
}
// Sets the title attribute of the logo on the WordPress login page
add_filter( 'login_headertitle', 'my_login_logo_url_title' );
function my_login_logo_url_title() {
    return '<Client Name>';
}
// Loads the custom CSS and JavaScript for the WordPress login page
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );
function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
    wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
}

?>