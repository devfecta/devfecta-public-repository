#!/bin/bash
sudo apt update
echo "Install Apache: Continue? (y/n)"
read answer
if [[ "${answer,,}" == 'y' ]]; then
    # Install Apache2 HTTP Server
    sudo apt install apache2
    # Configure Apache2
    # Standard Configuration
    # Confirm that your Ubuntu server configuration has Apache2 DirectoryIndex directives 
    # for php defined. Apache2 must have index.php as a directory index in its dir.conf file.
    #echo "Make sure index.php is defined as a DirectoryIndex in Apache2's dir.conf file: Continue? (y/n)"
    #read answer
    #if [[ "${answer,,}" == 'y' ]]; then
    #    sudo nano /etc/apache2/mods-enabled/dir.conf
    #fi
    # Configure your domain or server name for your site. Open Apache2 default site config 
    # file by running the commands below.
    echo "Add the 'ServerName localhost' and 'ServerAlias localhost' to match your domain name and save the file. Continue? (y/n)"
    read answer
    if [[ "${answer,,}" == 'y' ]]; then
       sudo nano /etc/apache2/sites-enabled/000-default.conf
    fi
    
    # Restart Apache2
    sudo systemctl restart apache2.service
fi
# MySQL Database Server
echo "Install MySQL? (y/n)"
read answer
if [[ "${answer,,}" == 'y' ]]; then
    # Install MySQL Database Server
    sudo apt install mysql-server
    # Lockdown MySQL
    echo "Running mysql_secure_installation"
    sudo mysql_secure_installation
    # Configure root for MySQL access.
    echo "For root access without sudo you need to configure root access."
    echo "Configure root for MySQL access? (y/n)"
    read answer
    if [[ "${answer,,}" == 'y' ]]; then
        #echo "Enter the root MySQL password, and hit ENTER:"
        read -p "Enter the root MySQL password, and hit ENTER:" password
        sudo mysql --password="$password" --execute="ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$password'; FLUSH PRIVILEGES;"
    fi
fi
# PHP and Related Modules
echo "Install PHP and standard PHP modules? (y/n)"
read answer
if [[ "${answer,,}" == 'y' ]]; then
    sudo apt install php php-mysql php-cgi libapache2-mod-php php-common php-pear php-mbstring php-gd php-gettext php-curl php-json
    sudo a2enconf php7.2-cgi

    sudo bash -c "echo -e '<?php\nphpinfo();\n?>' > /var/www/html/index.php"

    #sudo chmod -R 777 /var/www/html
    #sudo echo -e "<?php phpinfo(); ?>" > /var/www/html/index.php
    #sudo chmod -R 755 /var/www/html
    #sudo chown -R root /var/www/html
    #sudo chgrp -R root /var/www/html
    sudo systemctl reload apache2.service
fi
# Restart Apache Server
sudo systemctl restart apache2

# Install Git
echo "Install Git? (y/n)"
read answer
if [[ "${answer,,}" == 'y' ]]; then
    # For Ubuntu, this PPA provides the latest stable upstream Git version
    sudo add-apt-repository ppa:git-core/ppa
    sudo apt update
    sudo apt -y install git
    echo "Configure Git? (y/n)"
    read answer
    if [[ "${answer,,}" == 'y' ]]; then
        echo "Enter Git username:"
        read username
        git config --global user.name "$username"
        echo "Enter Git username:"
        read email
        git config --global user.email "$email"
    fi
fi

echo "Install Java 12? (y/n)"
read answer
if [[ "${answer,,}" == 'y' ]]; then
sudo add-apt-repository ppa:linuxuprising/java
sudo apt-get update
# To install Oracle Java 12 on Ubuntu 19.04:
sudo apt install oracle-java12-installer
fi

# Install Google Chrome
echo "Install Google Chrome? (y/n)"
read answer
if [[ "${answer,,}" == 'y' ]]; then
    wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
    sudo dpkg -i google-chrome-stable_current_amd64.deb
fi

# Install Slack
echo "Install Slack? (y/n)"
read answer
if [[ "${answer,,}" == 'y' ]]; then
    sudo snap install slack --classic
fi

# Install Visual Studio Code
echo "Install Visual Studio Code? (y/n)"
read answer
if [[ "${answer,,}" == 'y' ]]; then
    sudo snap install code --classic
fi