# Linux Stacks
## LAMP
The file install-apps.sh creates a LAMP stack with Git and Slack

### Install Adminer (Replacement for PHPMyAdmin)
1. Go to: [www.adminer.org](https://www.adminer.org) and select the Download button.

2. Under the /var/www/html directory, create an ‘adminer’ directory, and copy the adminer.php file you downloaded to the ‘adminer’ directory and name it ‘index.php’

3. Open a browser tab and point it to: http://localhost/adminer
    * Enter <a username> for Username
    * Enter <a password> for Password